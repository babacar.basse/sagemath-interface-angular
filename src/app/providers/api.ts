import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class ApiProvider {
  private apiUrl: string = 'http://aleph.sagemath.org/service';

  constructor(private http: HttpClient) {
  }

  hillCrypto(alphabet, text, matrix, matriceSize) {
    const code = `    S = ${alphabet}\n\
    E = HillCryptosystem(S,${matriceSize})\n\
    R = IntegerModRing(26)\n\
    M = MatrixSpace(R,${matriceSize},${matriceSize})\n\
    A = M(${matrix})\n\
    e = E(A)\n\
    print e(S("${text}"))`;
    const body = new HttpParams()
      .set('code', code);
    return this.http.post(this.apiUrl,
      body.toString(),
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
      }
    );
  }
}
