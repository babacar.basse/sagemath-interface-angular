import { Component, OnInit } from '@angular/core';
import {ApiProvider} from "../providers/api";
import {el} from "@angular/platform-browser/testing/src/browser_util";
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  hill = {
    text: '',
    matriceSize: 4,
    alphabet: 'Radix64Strings()'
  };
  constructor(
    private apiService: ApiProvider
  ) { }

  ngOnInit() {
    $('select.dropdown').dropdown();
  }

  numberToArray = (input, fillIndex?: boolean) : Array<any> => {
    if (fillIndex) {
      // @ts-ignore
      return Array(input).fill().map((x,i)=>i);
    }
    return Array(input).fill(input);
  };

  crypter(mode) {
    switch (mode) {
      case 'hill':
        const matrix = this.makeHillMatrix();
        if (!matrix) {
          return;
        }
        if (this.hill.text.length%this.hill.matriceSize) {
          alert(`La longueur du text doit etre un multiple de ${this.hill.matriceSize}, taille de la matrice`);
          return;
        }
        this.apiService.hillCrypto(this.hill.alphabet, this.hill.text, matrix, this.hill.matriceSize)
          .subscribe((resp: any) => {
            if (resp.stdout)
              document.getElementById('hillResult').innerHTML = resp.stdout;
            else
              document.getElementById('hillResult').innerHTML = 'Erreur lors du cryptage.';
          });
    }
  }

  makeHillMatrix() {
    let errorMatrix: boolean = false;
    let key_matrix = [];
    this.numberToArray(this.hill.matriceSize, true).forEach((i) => {
      let matrice = [];
      this.numberToArray(this.hill.matriceSize, true).forEach((j) => {
        let cell: any = document.getElementById(`hillmatrice_${i}${j}`);
        if (!cell.value) {
          errorMatrix = true;
          return;
        }
        matrice.push(cell.value);
      });
      key_matrix.push(matrice);
    });
    if (errorMatrix) {
      alert('Merci de remplir correctement la matrice');
      return false;
    }

    return JSON.stringify(key_matrix);
  }
}
